#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define S_BUFSIZE 32
/* #define INDENT    "\t" */
#define VERSION 0.8

/* Symbol definitions */
enum {
  OBRACE = '(',
  CBRACE = ')',
  VERBAT = '\'',
  IGNORE = ';',
  STRING = '"',
  KEY    = ':',
  ESCAPE = '\\',
  IMPORT = '%'
};

void    die      (const char *);
int     tgetc    (FILE *);
FILE*   xfreopen (const char *, const char *, FILE *);
int     buftil   (char *, FILE *, char []);
int     outtil   (char *, FILE *, FILE *);
int     peek     (FILE *, char, int);
void    ignore   (FILE *);
void    verbatim (FILE *, FILE *);
void    string   (FILE *, FILE *);
void    key      (FILE *, FILE *);
void    tag      (FILE *, FILE *);
void    parse    (FILE *, FILE *);
int     main     (int, char **);

void
die(const char *s)
{
  printf("wisp: %s\n", s);
  exit(1);
}

int
tgetc(FILE *in)
{
  static int stringp = 0;
  int c = fgetc(in);

  if (c == STRING)
    stringp = !stringp;

  if (stringp && c == ESCAPE)
    c = fgetc(in);

  return c;
}

FILE *
xfreopen(const char *filename, const char *mode, FILE *stream)
{
  FILE *fd = freopen(filename, mode, stream);

  if (!fd)
    perror("wisp"), exit(1);

  return fd;
}

int
buftil(char *chars, FILE *in, char buf[])
{
  int c; int i = 0; char *tp;

  while (i < S_BUFSIZE && (c = tgetc(in))) {
    for (tp = chars; *tp != 0; tp++) {
      if (c == *tp)
        goto end;
    }

    buf[i++] = c;
  }

end:
  buf[i] = 0; /* End the str and ensure it doesn't contain ch */
  return c;
}

int
outtil(char *chars, FILE *in, FILE *out)
{
  int c; char *tp;

  while ((c = tgetc(in))) {
    for (tp = chars; *tp != 0; tp++) {
      if (c == *tp)
        goto end;
    }
    fputc(c, out);
  }
end:
  return c;
}

/* void */
/* indent(int depth, FILE *out) */
/* { */
/*   while (depth-- > 0) */
/*     fprintf(out, "%s", INDENT); */
/* } */

int
peek(FILE *in, char c, int rspace)
{
  int ch = tgetc(in);
  if (rspace) {
    while (!isspace(c) && isspace(ch))
      ch = tgetc(in);
  }

  if (c == ch) {
    ungetc(ch, in);
    return 1;
  }

  ungetc(ch, in);
  return 0;
}

void
ignore(FILE *in)
{
  int c;
  while ((c = tgetc(in)) > 0 && c != '\n')
    ;
}

void
verbatim(FILE *in, FILE *out)
{
  while (outtil("'\\", in, out) == ESCAPE)
    fputc(tgetc(in), out);
}

void
string(FILE *in, FILE *out)
{
  fputc(tgetc(in), out); /* Output the first '"' */
  outtil("\"", in, out);
  fputc('"', out);
}

void
key(FILE *in, FILE *out)
{
  if (!peek(in, KEY, 1)) {
    return;
  } else {
    tgetc(in);
  }

repeat:
  fputc(' ', out);
  outtil(" \"", in, out);
  fputc('=', out);
  string(in, out);

  if (peek(in, KEY, 1)) {
    tgetc(in); /* clobber the leading ':' */
    goto repeat;
  }
}

void
tag(FILE *in, FILE *out)
{
  char buf[S_BUFSIZE];
  int c;

  if (peek(in, OBRACE, 0))
    tgetc(in);

  if ((c = buftil(" \n(", in, buf)) == KEY || c == ' ') {
    fprintf(out, "<%s", buf);
    key(in, out);
    fprintf(out, ">");
  } else {
    if (c != '\n')
      ungetc(c, in); /* considered harmful */
    fprintf(out, "<%s>", buf);
  }

  parse(in, out);

  fprintf(out, "</%s>", buf);
}

void
parse(FILE *in, FILE *out)
{
  static int c;

repeat:
  c = tgetc(in);

  switch (c) {
  case IGNORE:
    ignore(in);
    goto repeat;

  case VERBAT:
    verbatim(in, out);
    goto repeat;

  case OBRACE:
    tag(in, out);
    goto repeat;

  default:
    goto repeat;

  case CBRACE: /* FALL THROUGH */
  case 0:
  case EOF:
    break;
  }
}

int
main(int argc, char **argv)
{
  if (argc > 1) {
    if (strcmp(argv[1], "--help") == 0) {
      printf("wisp -- A program to convert s-exprs -> xml\n"
              "Version: %g; Author: Finn O'leary (@gallefray)\n\n"
              "Usage: wisp [infile outfile]\n"
              "  By default it takes input from stdin and outputs to stdout\n"
              "\nSyntax:\n"
              "  The accepted syntax is of the form:\n\n"
              "    (term [:keyname \"value\" ...] ['content'])\n\n"
              "  which outputs:\n\n"
              "    <term keyname=\"value\" ...>content</term>\n\n"
              "Please send bug reports to finnoleary@inventati.org\n",
              VERSION);
      return 0;
    }
    else {
      xfreopen(argv[1], "r", stdin);
      if (argv[2])
        xfreopen(argv[2], "w", stdout);
    }
  }
  while (!feof(stdin))
    parse(stdin, stdout);

  fputc('\n', stdout);

  exit(0);
}
