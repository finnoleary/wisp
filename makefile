OFILE=wisp
COMPILE=gcc -std=c11 -pedantic wisp.c -o $(OFILE) -Wall 2>&1

build:
		$(COMPILE)

small:
		$(COMPILE) -Os

clean:
		rm $(OFILE).o $(OFILE)

